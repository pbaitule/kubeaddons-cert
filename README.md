# kubeaddons

Custom addon repository to bootstrap deployment with Konvoy.

[Addon repositories - D2IQ documentation](https://docs.d2iq.com/ksphere/konvoy/1.5/tutorials/addon-repositories/)
